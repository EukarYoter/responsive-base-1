$('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', '/systemscripts/jetpacks/dynamic-cart/jetpack_dynamic-cart.css') );
$.getScript("/systemscripts/jetpacks/dynamic-cart/jetpack_dynamic-cart.js", function(){
    initJetpackDynamicCart({
        testing: false,
        isDropdown: false,
        dropdownFixedOnScroll: false,
        showTotalVat: true,
        showCartOnBuyClick: true
    });
});


