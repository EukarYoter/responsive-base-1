var catNav = {
    initTopMenu: function () {

        var catNav = $("#cat-nav");
        var navBar = catNav.children(".nav-bar");

        // TOP MENU STYLE
        catNav.addClass("style-vertical-right");
        catNav.find("ul.lv3").wrap("<div class='positioner'></div>");

        // MAKE SLIGHT DELAY ON HOVER
        var hoverAction;
        catNav.on("mouseenter", function () {
            clearTimeout(hoverAction);
            hoverAction = setTimeout(function () {
                catNav.addClass("hovered");
            }, 100);
        }).on("mouseleave", function () {
            clearTimeout(hoverAction);
            catNav.removeClass("hovered");
            catNav.checkTopMenuLayout();
        });

        // ADD TOUCH SUPPORT FOR DESKTOP
        if (J.checker.isTouch && Foundation.utils.is_large_up()) {

            var expandingElements = $("#cat-nav li.lv1.has-subcategories > a, #cat-nav li.lv2.has-subcategories > a");
            expandingElements.click(function (event) {
                var clickedNodeLink = $(this);
                event.preventDefault();

                if (!clickedNodeLink.parent().hasClass("clicked-once")) {
                    $("#cat-nav .clicked-once").not(clickedNodeLink.closest(".lv1")).removeClass("clicked-once");
                    $(this).parent().addClass("clicked-once");
                }
                else {
                    location.href = $(this).attr("href");
                }
            });

            $("body").on("click", function (event) {
                var target = $(event.target);
                if ($("#cat-nav .clicked-once").length && !target.closest("#cat-nav").length) {
                    $("#cat-nav .clicked-once").removeClass("clicked-once");
                }
            });
        }

        // CHECK LV 2 POS SO IT DOES NOT RENDER OUTSIDE OF SCREEN
        // FOR APPLICABLE TOP MENU TYPES
        catNav.checkLv2HorizontalPosition = function () {
            if (Foundation.utils.is_large_up()) {
                catNav.addClass("resizing");
                var windowWidth = $(window).width();
                // LOOP THROUGH LV1 TO FIND LV2 CONTAINERS THAT ARE OUTSIDE VISIBLE WINDOW
                catNav.find(".lv1.has-subcategories").each(function () {
                    var lv1Container = $(this);
                    var lv1OffsetLeft = lv1Container.offset().left;
                    var lv2Container = lv1Container.find("ul.lv2");
                    var lv2Width = lv2Container.outerWidth();
                    var overflow = windowWidth - lv1OffsetLeft - lv2Width;
                    if (overflow < 0) {
                        lv2Container.addClass("moved-horizontally").css("left", (overflow - 10));
                    }
                    else {
                        lv2Container.removeClass("moved-horizontally").css("left", "0px");
                        // IF VERTICAL-RIGHT ALSO CHECK FOR LV3 OUTSIDERS
                        var lv3Outside = false;
                        var lv3Containers = lv1Container.find("ul.lv3");
                        lv3Containers.each(function () {
                            var lv3Container = $(this);
                            var lv3Width = lv3Container.outerWidth();
                            //log(lv3Width);
                            if ((overflow - lv3Width) < 0) {
                                lv3Outside = true;
                                return false;
                            }
                        });
                        if (lv3Outside) {
                            lv2Container.addClass("lv3-to-left");
                        }
                        else {
                            lv2Container.removeClass("lv3-to-left");
                        }
                    }
                });
                catNav.removeClass("resizing");
            }
        };

        // STOP CLICK PROPAGATION FOR LINKS
        catNav.on("click", "a", function (event) {
            event.stopPropagation();
        });

        // TOP CAT MENU SPACE CALCULATION
        var lastWindowHeight = 0;
        catNav.checkTopMenuLayout = function () {

            if (Foundation.utils.is_large_up()) {
                var winScrollTop = $(window).scrollTop();
                var catNavTopOffset = catNav.offset().top; // HEIGHT UNTIL TOP EDGE OF HOVER DYNAMIC CAT MENU CONTAINER
                var navBarHeight = navBar.height(); // HEIGHT OF TOP MENU BAR
                // CHECK IF TOP MENU BAR SHOULD BE FIXED
                if (winScrollTop > (catNavTopOffset + navBarHeight)) { // IS SCROLLED
                    if (!catNav.is(".hovered")) {
                        catNav.height(navBarHeight);
                        $("html").addClass("menu-scrolled");
                        setTimeout(function () {
                            $("html").removeClass("menu-static");
                        }, 10);
                    }
                }
                else { // RETURN TO NORMAL
                    $("html").removeClass("menu-scrolled");
                    catNav.css("height", "auto");
                    setTimeout(function () {
                        $("html").addClass("menu-static");
                    }, 10);
                }
            }
        };

        // CHECK MENU LAYOUT
        catNav.checkTopMenuLayout();
        catNav.checkLv2HorizontalPosition();

        // BIND DYNAMIC MENU SPACE CALC TO RESIZE
        var resizeAction;
        $(window).resize(function () {
            clearTimeout(resizeAction);
            resizeAction = setTimeout(function () {

                // RESIZE DESKTOP MENU
                catNav.checkLv2HorizontalPosition();
                catNav.checkTopMenuLayout();
            }, 100);
        });

        var scrollAction;
        $(window).scroll(function () {
            clearTimeout(scrollAction);
            scrollAction = setTimeout(function () {

                // RESIZE DESKTOP MENU
                catNav.checkTopMenuLayout();
            }, 100);
        });

        // THIS RESETS MENU WHEN GOING FROM MEDIUM TO LARGE
        // FIRED FROM J.switch.toLargeUp
        catNav.resetMobileMenuStyles = function () {
            catNav.add(navBar).removeAttr("style");
        };

        // ADD CHECK FOR MOBILE STYLING WHEN IN LARGE-UP
        J.switch.addToLargeUp(catNav.resetMobileMenuStyles);

    }
};
J.pages.addToQueue("all-pages", catNav.initTopMenu);