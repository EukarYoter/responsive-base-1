﻿var dynamicCart = {
    //TODO animation on delete item? Async, happening too fast?
    settings: {
        type: "modal", // "modal" or "dropdown"
        showTotalVat: true,
        topDistance: 100,
        topDistanceSmall: 30
    },

    lastProductAdded: 0,

    dcWrapper: false,

    render: function () {
        // Save the ID for last product added to cart, used for animation
        J.cart.lastProductAdded = dynamicCart.lastProductAdded;
        // Items
        var itemTemplate = J.views['dynamic-cart/dynamic-cart-item'];
        var itemHtml = itemTemplate(J.cart);
        $("#dc-content").html(itemHtml);
        // Totals
        var totalSumData = {
            TotalProductSumText: J.cart.TotalProductSumText,
            TotalProductSumNoVatText: J.cart.TotalProductSumNoVatText,
            TotalProductVatSumText: J.cart.TotalProductVatSumText,
            showTotalVat: dynamicCart.settings.showTotalVat
        };
        var totalsTemplate = J.views['dynamic-cart/dynamic-cart-totals'];
        var totalsHtml = totalsTemplate(totalSumData);
        $("#dc-totals").html(totalsHtml);
        dynamicCart.dcWrapper.removeClass("loading").removeClass("deleting");
        setTimeout(function(){
            $(".dc-item-row").removeClass("product-added"); // This class is added in view to highlight last product added
        }, 300);
    },

    open: function (isCartAddOpening) {
        if (isCartAddOpening) {
            dynamicCart.dcWrapper.addClass("loading");
        }
        if (dynamicCart.settings.type === "modal" || Foundation.utils.is_small_only()) {
            if (Foundation.utils.is_small_only()) {
                dynamicCart.dcWrapper.data().cssTop = dynamicCart.settings.topDistanceSmall;
            } else {
                dynamicCart.dcWrapper.data().cssTop = dynamicCart.settings.topDistance;
            }
            dynamicCart.dcWrapper.foundation("reveal", "open");
        }
        else if (dynamicCart.settings.type === "dropdown") {
            dynamicCart.dcWrapper.addClass("dropdown-open").find("#dc-inner").stop(true, true).slideDown("fast");
        }
    },

    close: function () {
        dynamicCart.dcWrapper.removeClass("loading");
        dynamicCart.dcWrapper.foundation("reveal", "close"); // Always close modal
        if (dynamicCart.settings.type === "dropdown") {
            dynamicCart.dcWrapper.removeClass("dropdown-open").find("#dc-inner").stop(true, true).slideUp("fast");
        }
    },

    bind: function () {
        // Bind reveal closed to purge style attributes from cart wrapper
        $(document).on("closed.fndtn.reveal", '[data-reveal]', function () {
            var modal = $(this);
            modal.removeAttr("style");
        });
        // Bind cart click
        $(".small-cart-body").click(function () {
            if (dynamicCart.settings.type === "modal") {
                dynamicCart.open(false);
            }
            else if (dynamicCart.settings.type === "dropdown") {
                if (dynamicCart.dcWrapper.hasClass("dropdown-open")) {
                    dynamicCart.close();
                }
                else {
                    dynamicCart.open();
                }
            }
        });
        // Bind continue button
        $("#dc-continue-btn").click(function (event) {
            event.preventDefault();
            dynamicCart.close();
        });
        // Bind delete button
        dynamicCart.dcWrapper.on("click", ".delete i", function (event) {
            event.preventDefault();
            var clickedBtn = $(this);
            var prodRow = clickedBtn.closest(".dc-item-row");
            var prodRowHeight = prodRow.height();
            var recId = prodRow.attr("data-rec-id");
            prodRow.addClass("deleting").slideUp(300, "easeInOutCubic");
            dynamicCart.dcWrapper.addClass("deleting");
            J.components.deleteFromCart(recId);
        });
    },

    init: function () {
        J.translations.push(
            {
                dcProdLoadMsg: {
                    sv: "Lägger varan i kundvagnen...",
                    nb: "Legger vare i handlekurven...",
                    da: "Lægger varen i indkøbsvognen...",
                    fi: "Tuote lisätään ostoskoriin...",
                    de: "Artikel zum Warenkorb hinzufügen...",
                    en: "Adding item to cart..."
                },
                dcCartLoadMsg: {
                    sv: "Hämtar kundvagnen...",
                    nb: "Henter handlekurven...",
                    da: "Henter indkøbsvogn...",
                    fi: "Ostoskorille siirrytään...",
                    de: "Zum Warenkorb...",
                    en: "Getting cart..."
                },
                dcContinueBtnText: {
                    sv: "Fortsätt handla",
                    nb: "Handle mer",
                    da: "Fortsæt med at shoppe",
                    fi: "Jatka ostoksia",
                    de: "Mit dem Einkauf fortfahren",
                    en: "Continue shopping"
                },
                dcCheckoutBtnText: {
                    sv: "Till kassan",
                    nb: "Gå til kassen",
                    da: "Gå til kassen",
                    fi: "Mene kassalle",
                    de: "Zur Kasse",
                    en: "Proceed to checkout"
                },
                dcCartHeader: {
                    sv: "Din kundvagn",
                    nb: "Din handlekurv",
                    da: "Din kundevogn",
                    fi: "Ostoskorisi",
                    de: "Ihr Warenkorb",
                    en: "Your shopping cart"
                },
                dcItem: {
                    sv: "Artikel",
                    nb: "Artikkel",
                    da: "Vare",
                    fi: "Tuote",
                    de: "Artikel",
                    en: "Article"
                },
                dcPrice: {
                    sv: "Pris",
                    nb: "Pris",
                    da: "Pris",
                    fi: "Hinta",
                    de: "Preis",
                    en: "Price"
                },
                dcQty: {
                    sv: "Antal",
                    nb: "Antall",
                    da: "Antal",
                    fi: "Määrä",
                    de: "Anzahl",
                    en: "Qty"
                },
                dcTotal: {
                    sv: "Summa",
                    nb: "Summer",
                    da: "I alt",
                    fi: "Summa",
                    de: "Summe",
                    en: "Total"
                },
                dcItemTotal: {
                    sv: "Summa artiklar",
                    nb: "Summer artikler",
                    da: "Varer i alt",
                    fi: "Yhteensä tuotteita",
                    de: "Summe Artikel",
                    en: "Total items"
                },
                dcOfWhichVat: {
                    sv: "varav moms",
                    nb: "herav mva",
                    da: "heraf moms",
                    fi: "josta alv:n osuus",
                    de: "davon MwSt",
                    en: "of which VAT"
                },
                dcPlusVat: {
                    sv: "moms tillkommer med",
                    nb: "+ moms",
                    da: "der tillægges moms på",
                    fi: "hinta sisältää alv:n",
                    de: "+ MwSt",
                    en: "+ VAT"
                },
                dcItemNumber: {
                    sv: "Artikelnummer",
                    nb: "Artikkelnummer",
                    da: "Varenummer",
                    fi: "Tuotenumero",
                    de: "Artikelnummer",
                    en: "Item number"
                },
                dcViewCart: {
                    sv: "Se kundvagn",
                    nb: "Se handlekurv",
                    da: "Se indkøbsvogn",
                    fi: "Näytä ostoskori",
                    de: "Warenkorb anzeigen",
                    en: "View cart"
                }
            }
        );

        // Render cart container if not already rendered
        if (!$("#dc-wrapper").length) {
            var template = J.views['dynamic-cart/dynamic-cart'];
            var html = template(null);
            $(".cart-area-wrapper").append(html);
        }
        dynamicCart.dcWrapper = $("#dc-wrapper");

        // Set type as html class
        if (dynamicCart.settings.type === "modal") {
            $("html").addClass("dynamic-cart-modal");
        }
        else if (dynamicCart.settings.type === "dropdown") {
            $("html").addClass("dynamic-cart-dropdown");
        }

        // Bind
        dynamicCart.bind();

        // Hijack add to cart function
        var oldAddToCart = JetShop.StoreControls.Services.General.AddCartItem;
        JetShop.StoreControls.Services.General.AddCartItem = function () {
            console.log(arguments[0].ProductID);
            dynamicCart.lastProductAdded = arguments[0].ProductID;
            dynamicCart.open(true);
            return oldAddToCart.apply(this, arguments);
        };

        J.switch.addToMediumUp(function () {
            if (dynamicCart.settings.type === "dropdown") {
                dynamicCart.close();
                dynamicCart.dcWrapper.removeAttr("style");
            }
        });
        J.switch.addToSmall(function () {
            if (dynamicCart.settings.type === "dropdown") {
                dynamicCart.close();
            }
        });

        $(window).on("cart-updated", function () {
            dynamicCart.render();
        });
    }
};

J.pages.addToQueue("all-pages", dynamicCart.init);
