// INIT MOBILE MENU -------------
var catNavMobile = {

    settings: {
        autoCloseInactiveMobileNodes: true
    },

    checkMobileMenuLayout: function (isOpening) {
        var catNav = $("#cat-nav");
        if (!Foundation.utils.is_large_up()) {
            var winScrollTop = $(window).scrollTop();
            var catNavTopOffset = catNav.offset().top; // HEIGHT UNTIL TOP EDGE OF HOVER DYNAMIC CAT MENU CONTAINER
            var navBar = catNav.children(".nav-bar");
            var navBarTopOffset = navBar.offset().top;
            var navBarHeight = navBar.height(); // HEIGHT OF TOP MENU BAR
            var viewportHeight = $(window).height();
            var menuWrapper = $("#menu-wrapper");
            var menuHeight = menuWrapper.height();
            var menuTopOffset;
            // CHECK IF TOP MENU BAR SHOULD BE FIXED
            if (winScrollTop > (catNavTopOffset - menuHeight)) { // IS SCROLLED
                catNav.height(menuHeight);
                $("html").addClass("menu-scrolled");
                setTimeout(function () {
                    $("html").removeClass("menu-static");
                }, 10);
            }
            // RETURN TO NORMAL
            else {
                catNav.css("height", "auto");
                $("html").removeClass("menu-scrolled");
                setTimeout(function () {
                    $("html").addClass("menu-static");
                }, 10);
            }
            // SCROLL CAT NAVIGATION (TIMEOUT TO WAIT FOR DOM CHANGES)
            if ($("html.menu-open").length || isOpening) {
                setTimeout(function () {
                    // TAKE FIXED/NOT FIXED STATE INTO ACCOUNT
                    if ($("html.menu-scrolled").length) {
                        menuTopOffset = winScrollTop;
                    }
                    else {
                        menuTopOffset = menuWrapper.offset().top;
                    }
                    // IF LEFT AREA TOP BELOW CURRENT SCROLL POSITION - MOVE UP
                    if (navBarTopOffset > (menuTopOffset + menuHeight)) {
                        navBar.css("top", (menuTopOffset + menuHeight + 10) + "px");
                    }
                    // IF LEFT AREA TOP ABOVE SCROLL POSITION AND THERE IS SPACE ENOUGH TO FIT IT, MOVE DOWN
                    else if ((viewportHeight - menuHeight) > navBarHeight) {
                        navBar.css("top", (menuTopOffset + menuHeight + 10) + "px");
                    }
                    // IF END OF LONG LEFT AREA ABOVE BOTTOM OF PAGE - MOVE DOWN
                    else {
                        var posDiff = (winScrollTop + viewportHeight) - (navBarTopOffset + navBarHeight);
                        if (posDiff > 150) {
                            navBar.css("top", (navBarTopOffset + posDiff - 10) + "px");
                        }
                    }
                }, 5);
            }
        }
    },

    init: function () {
        var catNav = $("#cat-nav");
        // EXPANDING CLICKS IN MEDIUM-DOWN
        catNav.on("click", "ul, li", function (event) {
            if (!Foundation.utils.is_large_up()) {
                event.stopPropagation();
                var clickedNode = $(this);
                if (clickedNode.is("li.has-subcategories")) {
                    clickedNode.toggleClass("open");
                    if (catNavMobile.settings.autoCloseInactiveMobileNodes) {
                        // CLOSE ALL OPEN NODES NOT DIRECT PARENT OF CLICKED NODE
                        catNav.find(".open").not(clickedNode).not(clickedNode.parentsUntil("#cat-nav")).removeClass("open");
                    }
                    else {
                        // ONLY CLOSE OPEN NODES THAT ARE DIRECT DESCENDANTS OF CLOSED NODE
                        clickedNode.find(".open").removeClass("open");
                    }
                }
            }
        });
        // STOP CLICK PROPAGATION FOR LINKS
        catNav.on("click", "a", function (event) {
            event.stopPropagation();
        });
        // BIND DYNAMIC MENU SPACE CALC TO RESIZE AND SCROLL
        var resizeAction;
        $(window).resize(function () {
            clearTimeout(resizeAction);
            resizeAction = setTimeout(function () {
                catNavMobile.checkMobileMenuLayout();
            }, 100);
        });
        var scrollAction;
        $(window).scroll(function () {
            clearTimeout(scrollAction);
            scrollAction = setTimeout(function () {
                catNavMobile.checkMobileMenuLayout();
            }, 100);
        });
    }
};

J.pages.addToQueue("all-pages", catNavMobile.init);
